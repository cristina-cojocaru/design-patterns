from functools import wraps


# Define a decorator function, which takes another function as an argument
def my_decorator(my_func):
    @wraps(
        my_func)  # @wraps preserves function metadata like name and docstring
    def wrapper(*args, **kwargs):
        # do something before my_func is called
        print("Doing something first.")

        result = my_func(*args, **kwargs)

        # do something after my_func is called
        print("Doing something after.")

        return result

    return wrapper


@my_decorator  # invoke decorator
def my_func(x, y):
    """This function returns the sum of x and y"""
    print("my_func is running.")
    print(x+y)
    return x + y


my_func(5, 3)