from abc import ABC, abstractmethod
import pathlib


class VideoExporter(ABC):

    @abstractmethod
    def prepare_export(self, video_data):
        """ Prepares video data for exporting """

    @abstractmethod
    def do_export(self, folder: pathlib.Path):
        """ Exports the video to a folder """


class LosslessVideoExporter(VideoExporter):
    """ Losless video exporting codec. """

    def prepare_export(self, video_data):
        print("Prepares video data for lossless exporting")

    @abstractmethod
    def do_export(self, folder: pathlib.Path):
        print(f"Exports the video to a folder: {folder}")


class H264BPVideExporter(VideoExporter):
    """ H264BP video exporting codec. """

    def prepare_export(self, video_data):
        print("Prepares video data for H264BP exporting")

    def do_export(self, folder: pathlib.Path):
        print(f"Exports the video to a folder: {folder}")


class AudioExporter(ABC):

    @abstractmethod
    def prepare_export(self, audio_data):
        """ Prepares audio data for export """

    @abstractmethod
    def do_export(self, folder: pathlib.Path):
        """ Exports the audio data to a folder """


class AACAudioExporter(AudioExporter):
    def prepare_export(self, audio_data):
        print("Prepares audio data for AAC export")

    def do_export(self, folder: pathlib.Path):
        print(f"Exports the audio data to a folder: {folder}")


class WAVAudioExporter(AudioExporter):
    def prepare_export(self, audio_data):
        print("Prepares audio data for WAV export")

    def do_export(self, folder: pathlib.Path):
        print(f"Exports the audio data to a folder: {folder}")


class ExporterFactory(ABC):
    """ Factory that represents a combination of video and audio codecs.
    The factory doesn't maintain any of the instances it creates
    """
    def get_video_exporter(self) -> VideoExporter:
        """ Return a new video exporter instance """

    def get_audio_expoter(self) -> AudioExporter:
        """ Returns a new audio exporter instance """


class FastExporter(ExporterFactory):
    """ Factory providing a fast, lower quality export """
    def get_video_exporter(self) -> VideoExporter:
        return H264BPVideExporter()

    def get_audio_expoter(self) -> AudioExporter:
        return AACAudioExporter()


class HighQualityExporter(ExporterFactory):
    """ Factory providing a slower, high quality export """
    def get_video_exporter(self) -> VideoExporter:
        return LosslessVideoExporter()

    def get_audio_expoter(self) -> AudioExporter:
        return WAVAudioExporter()


def read_exporter() -> ExporterFactory:
    factories = {
        "low": FastExporter(),
        "high": HighQualityExporter()
    }
    export_quality = input('Enter desired output quality (low, high): ')
    if export_quality in ['low', 'high']:
        return factories[export_quality]


def main():

    factory = read_exporter()
    # retrieve the video and audio exporter

    video_exporter = factory.get_video_exporter()
    audio_exporter = factory.get_audio_expoter()

    video_exporter.prepare_export("video data")
    audio_exporter.prepare_export("audio data")

    folder = pathlib.Path('/SDA')
    video_exporter.do_export(folder)
    audio_exporter.do_export(folder)


if __name__ == "__main__":
    main()