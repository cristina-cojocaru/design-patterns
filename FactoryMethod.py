class Game:
    def get_name(self):
        pass

    def get_type(self):
        pass


class BoardGame(Game):
    def __init__(self, name, type):
        self.name = name
        self.type = type

    def get_name(self):
        return self.name

    def get_type(self):
        return self.type

    def __str__(self):
        return f"{__name__} [name='{self.name}', type='{self.type}']"


class PCGame(Game):
    def __init__(self, name, type):
        self.name = name
        self.type = type

    def get_name(self):
        return self.name

    def get_type(self):
        return self.type

    def __str__(self):
        return f"{__name__} [name='{self.name}', type='{self.type}']"


class GameFactory:
    def create(self):
        pass


class MonopolyGameCreator(GameFactory):
    def create(self):
        return BoardGame("Monopoly", "Family Game")


class ValorantGameCreator(GameFactory):
    def create(self):
        return PCGame("LOL", "Online game")


def main():
    game_type = input('Enter the type of game [PC, Board]: ')
    game_factory = None
    if game_type == 'PC':
        game_factory = ValorantGameCreator()
    elif game_type == 'Board':
        game_factory = MonopolyGameCreator()

    if game_factory:
        game = game_factory.create()
        print(game)


if __name__ == '__main__':
    main()
