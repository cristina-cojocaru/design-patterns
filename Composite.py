class Element:
    def get_name(self):
        pass

    def get_salary(self):
        pass

    def get_vacation_days(self):
        pass


class Employee(Element):
    def __init__(self, name, salary, vacation_days):
        self._name = name
        self._salary = salary
        self._vacation_days = vacation_days

    def get_name(self):
        return self._name

    def get_salary(self):
        return self._salary

    def get_vacation_days(self):
        return self._vacation_days


class Department(Element):
    def __init__(self, name):
        self._name = name
        self._elements = []

    def get_name(self):
        return self._name

    def get_salary(self):
        salary = 0.0
        for el in self._elements:
            salary += el.get_salary()
        return salary

    def get_vacation_days(self):
        vacation_days = 0
        for el in self._elements:
            vacation_days += el.get_vacation_days()
        return vacation_days

    def add_element(self, element):
        self._elements.append(element)


def main():
    acme = Department('ACME')
    sales = Department('Sales')
    prod = Department('Production')
    acme.add_element(sales)
    acme.add_element(prod)
    acme.add_element(Employee('Marie Smith', 30000.0, 30))
    acme.add_element(Employee('John Kowal', 25000.0, 25))
    sales.add_element(Employee('Joanna Denver', 20000.0, 20))
    sales.add_element(Employee('Stephan Fox', 15000.0, 15))
    prod.add_element(Employee('Anna Brown', 10000.0, 10))
    prod.add_element(Employee('Mark Wayne', 5000.0, 5))

    print(f'Dep: {acme.get_name()}, salary: {acme.get_salary()}, vacation days: {acme.get_vacation_days()}')
    print(f'Dep: {sales.get_name()}, salary: {sales.get_salary()}, vacation days: {sales.get_vacation_days()}')
    print(f'Dep: {prod.get_name()}, salary: {prod.get_salary()}, vacation days: {prod.get_vacation_days()}')


if __name__ == '__main__':
    main()