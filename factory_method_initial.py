from abc import ABC, abstractmethod
import pathlib


class VideoExporter(ABC):

    @abstractmethod
    def prepare_export(self, video_data):
        """ Prepares video data for exporting """

    @abstractmethod
    def do_export(self, folder: pathlib.Path):
        """ Exports the video to a folder """


class LosslessVideoExporter(VideoExporter):
    """ Losless video exporting codec. """

    def prepare_export(self, video_data):
        print("Prepares video data for lossless exporting")

    @abstractmethod
    def do_export(self, folder: pathlib.Path):
        print(f"Exports the video to a folder: {folder}")


class H264BPVideExporter(VideoExporter):
    """ H264BP video exporting codec. """

    def prepare_export(self, video_data):
        print("Prepares video data for H264BP exporting")

    def do_export(self, folder: pathlib.Path):
        print(f"Exports the video to a folder: {folder}")


class AudioExporter(ABC):

    @abstractmethod
    def prepare_export(self, audio_data):
        """ Prepares audio data for export """

    @abstractmethod
    def do_export(self, folder: pathlib.Path):
        """ Exports the audio data to a folder """


class AACAudioExporter(AudioExporter):
    def prepare_export(self, audio_data):
        print("Prepares audio data for AAC export")

    def do_export(self, folder: pathlib.Path):
        print(f"Exports the audio data to a folder: {folder}")


class WAVAudioExporter(AudioExporter):
    def prepare_export(self, audio_data):
        print("Prepares audio data for WAV export")

    def do_export(self, folder: pathlib.Path):
        print(f"Exports the audio data to a folder: {folder}")


def main():
    export_quality = input('Enter desired output quality (low, high): ')
    if export_quality not in ['low', 'high']:
        print('Unknown format written by the user')
    else:
        if export_quality == 'low':
            video_exporter = H264BPVideExporter()
            audio_exporter = AACAudioExporter()
        else:
            video_exporter = LosslessVideoExporter()
            audio_exporter = WAVAudioExporter()
        video_exporter.prepare_export("video data")
        audio_exporter.prepare_export("audio data")

        folder = pathlib.Path('/SDA')
        video_exporter.do_export(folder)
        audio_exporter.do_export(folder)


if __name__ == "__main__":
    main()