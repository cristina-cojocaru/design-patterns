# Interface Segregation
from abc import ABC, abstractmethod


class Order:
    items = []
    quantities = []
    prices = []
    status = "open"

    def add_item(self, name, quantity, price):
        self.items.append(name)
        self.quantities.append(quantity)
        self.prices.append(price)

    def total_price(self):
        total = 0
        for i in range(len(self.prices)):
            total += self.quantities[i] * self.prices[i]
        return total


class PaymentProcessor(ABC):
    @abstractmethod
    def pay(self, order):
        pass


class PaymentProcessor_SMS(PaymentProcessor):
    @abstractmethod
    def auth_sms(self, code):
        pass


class DebitPaymentProcessor(PaymentProcessor_SMS):
    def __init__(self, security_code):
        self.security_code = security_code
        self.verified = False

    def pay(self, order):
        if not self.verified:
            raise Exception('Not authorized')
        print('Processing debit payment type')
        print(f'Verifying security code {self.security_code}')
        order.status = 'paid'

    def auth_sms(self, code):
        print(f'Verifying SMS code {code}')
        self.verified = True


class CreditPaymentProcessor(PaymentProcessor):
    def __init__(self, security_code):
        self.security_code = security_code

    def pay(self, order):
        print('Processing credit payment type')
        print(f'Verifying security code {self.security_code}')
        order.status = 'paid'


class PaypalPaymentProcessor(PaymentProcessor_SMS):
    def __init__(self, email_adress):
        self.email_adress = email_adress
        self.verified = False

    def pay(self, order):
        if not self.verified:
            raise Exception('Not authorized')
        print('Processing credit payment type')
        print(f'Verifying email adress {self.email_adress}')
        order.status = 'paid'

    def auth_sms(self, code):
        print(f'Verifying SMS code {code}')
        self.verified = True


order = Order()
order.add_item("Jeans", 1, 100)
order.add_item("Shirt", 2, 80)
print(order.status)
print(order.total_price())
processor = PaypalPaymentProcessor("email@email.com")
processor.auth_sms(65437)
processor.pay(order)
print(order.status)

order_2 = Order()
order_2.add_item('Dress', 1, 200)
print(order_2.status)
processor = DebitPaymentProcessor("657654")
processor.auth_sms(65437)
processor.pay(order_2)
print(order_2.status)
